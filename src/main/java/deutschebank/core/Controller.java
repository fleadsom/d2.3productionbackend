package deutschebank.core;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import deutschebank.MainUnit;
import deutschebank.dbutils.CounterParty;
import deutschebank.dbutils.CounterPartyHandler;
import deutschebank.dbutils.DBConnector;
import deutschebank.dbutils.Deal;
import deutschebank.dbutils.DealHandler;
import deutschebank.dbutils.DealIterator;
import deutschebank.dbutils.GraphOne;
import deutschebank.dbutils.GraphOneHandler;
import deutschebank.dbutils.GraphThree;
import deutschebank.dbutils.GraphThreeHandler;
import deutschebank.dbutils.GraphTwo;
import deutschebank.dbutils.GraphTwoHandler;
import deutschebank.dbutils.Instrument;
import deutschebank.dbutils.InstrumentHandler;
import deutschebank.dbutils.PropertyLoader;

public class Controller {
	private DBConnector connector = null;
	private PropertyLoader pLoader = null;
	private Properties pp = null;
	public Controller () {
		try {
			connector = DBConnector.getConnector();
			pLoader = PropertyLoader.getLoader();
			pp = pLoader.getPropValues( "dbConnector.properties" );
			connector.connect( pp );
		}catch (IOException ex) {
			Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	public String getAllInstrument() {
         InstrumentHandler theInstrumentHandler = InstrumentHandler.getLoader();
         ArrayList<Instrument> theInstruments = theInstrumentHandler.loadFromDB(pp.getProperty("dbName"), connector.getConnection());
         
         try {
        	 ObjectMapper mapper = new ObjectMapper();
        	 // Convert object to JSON string and pretty print
			 String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(theInstruments);
			 	return jsonInString;
		 } catch (JsonProcessingException e) {
			 Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, e);
		 }
         		return "failed";
	}
	
	public String getAllCounterparty() {
		CounterPartyHandler theCounterpartyHandler = CounterPartyHandler.getLoader();
        ArrayList<CounterParty> cps = theCounterpartyHandler.loadFromDB(pp.getProperty("dbName"), connector.getConnection());
        
        try {
       	 ObjectMapper mapper = new ObjectMapper();
       	 // Convert object to JSON string and pretty print
		 String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(cps);
			 	return jsonInString;
		 } catch (JsonProcessingException e) {
			 Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, e);
		 }
        		return "failed";
	}
	
	public String getAllDeal() {
		DealHandler theDealHandler = DealHandler.getLoader();
        ArrayList<Deal> theDeals = theDealHandler.loadFromDB(pp.getProperty("dbName"), connector.getConnection());

        try {
       	 ObjectMapper mapper = new ObjectMapper();
       	 // Convert object to JSON string and pretty print
		 String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(theDeals);
			 	return jsonInString;
		 } catch (JsonProcessingException e) {
			 Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, e);
		 }
        		return "failed";
	}
	
	public String getGraphOne() {
		GraphOneHandler theGraphOneHandler = GraphOneHandler.getLoader();
        ArrayList<GraphOne> theGraphOne = theGraphOneHandler.loadFromDB(pp.getProperty("dbName"), connector.getConnection());
        try {
       	 ObjectMapper mapper = new ObjectMapper();
       	 // Convert object to JSON string and pretty print
		 String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(theGraphOne);
			 	return jsonInString;
		 } catch (JsonProcessingException e) {
			 Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, e);
		 }
		return "failed getGraphOne";
	}
	
	public String getGraphTwo() {
		GraphTwoHandler theGraphTwoHandler = GraphTwoHandler.getLoader();
        ArrayList<GraphTwo> theGraphTwo = theGraphTwoHandler.loadFromDB(pp.getProperty("dbName"), connector.getConnection());
        try {
       	 ObjectMapper mapper = new ObjectMapper();
       	 // Convert object to JSON string and pretty print
		 String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(theGraphTwo);
		 System.out.println(jsonInString);
			 	return jsonInString;
			 	
			 	
		 } catch (JsonProcessingException e) {
			 Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, e);
		 }
		return "failed getGraphTwo";
	}
	
	public String getGraphThree(String counterPartyName) {
		GraphThreeHandler theGraphThreeHandler = GraphThreeHandler.getLoader();
        ArrayList<GraphThree> theGraphThree = theGraphThreeHandler.loadFromDB(pp.getProperty("dbName"), connector.getConnection(), counterPartyName);
        try {
       	 ObjectMapper mapper = new ObjectMapper();
       	 // Convert object to JSON string and pretty print
		 String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(theGraphThree);
			 	return jsonInString;
			 	
			 	
		 } catch (JsonProcessingException e) {
			 Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, e);
		 }
		return "failed getGraphThree";
	}
	
	//Controller ct = new Controller ();
	//ct.getRequirementOne("B");
	public String getRequirementOne(String dealType) {
        String  dbID = pp.getProperty("dbName");
        Connection theConnection = connector.getConnection();
        try
        {
            String sbQuery = "select sum(t1deal_amount) as sumDealAmount, sum(t1deal_quantity) as sumDealQuantity, t1instrument_name ";
            sbQuery = sbQuery+"from (select d.deal_type as t1deal_type , d.deal_amount as t1deal_amount, d.deal_quantity as t1deal_quantity , instr.instrument_name as t1instrument_name ";
            sbQuery = sbQuery+"from " + dbID + ".deal d join " + dbID + ".instrument instr on instrument_id=d.deal_instrument_id where d.deal_type=? ) as t1 group by t1instrument_name ";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);            
            stmt.setString(1, dealType);
            ResultSet rs = stmt.executeQuery();
            
            while( rs.next() )
            {
            	Double sumDealAmount = rs.getDouble("sumDealAmount");
            	int sumDealQuantity = rs.getInt("sumDealQuantity");
            	String t1instrument_name = rs.getString("t1instrument_name");
            	
            	System.out.println(t1instrument_name+" "+sumDealAmount+" "+sumDealQuantity);

            }
        } 
        catch (SQLException ex)
        {
        	Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "failed getRequirementOne";
	}
	
	public String getRequirementTwo() {
		return "failed getRequirementTwo";
	}
	
	public String getRequirementThree() {
		return "failed getRequirementThree";
	}
	
	public String getRequirementFour() {
		return "failed getRequirementFour";
	}
	}