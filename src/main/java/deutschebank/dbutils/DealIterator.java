package deutschebank.dbutils;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DealIterator {
	   ResultSet rowIterator;

	   DealIterator( ResultSet rs )
	   {
	       rowIterator = rs;
	   }
	   
	   public boolean  first() throws SQLException
	   {
	      return rowIterator.first();
	   }
	   
	   public boolean last() throws SQLException
	   {
	      return rowIterator.last();
	   }
	   public boolean next() throws SQLException
	   {
	      return rowIterator.next();
	   }
	   
	   public boolean prior() throws SQLException
	   {
	      return rowIterator.previous();
	   }

	   public   int  getDealID() throws SQLException
	   {
	      return rowIterator.getInt("deal_id");
	   }

	   public   String  getDealTime() throws SQLException
	   {
	      return rowIterator.getString("deal_time");
	   }
	   
	   public   int  getDealCounterpartyID() throws SQLException
	   {
	      return rowIterator.getInt("deal_counterparty_id");
	   }
	   
	   public   int  getDealInstrumentID() throws SQLException
	   {
	      return rowIterator.getInt("deal_instrument_id");
	   }
	   
	   public   String  getDealType() throws SQLException
	   {
	      return rowIterator.getString("deal_type");
	   }
	   
	   public   double  getDealAmount() throws SQLException
	   {
	      return rowIterator.getDouble("deal_amount");
	   }
	   
	   public   int  getDealQuantity() throws SQLException
	   {
	      return rowIterator.getInt("deal_quantity");
	   }
	   
	   public   String  getCounterPartyName() throws SQLException
	   {
	      return rowIterator.getString("counterparty_name");
	   }
	   
	   public   String  getCounterPartyStatus() throws SQLException
	   {
	      return rowIterator.getString("counterparty_status");
	   }
	   
	   public   String  getCounterPartyDateRegistered() throws SQLException
	   {
	      return rowIterator.getString("counterparty_date_registered");
	   }
	   
	   public   String  getInstrumentName() throws SQLException
	   {
	      return rowIterator.getString("instrument_name");
	   }

	   Deal   buildDeal() throws SQLException
	   {
		   CounterParty cp = new CounterParty(getDealCounterpartyID());
		   Instrument instru = new Instrument(getDealInstrumentID());
	       Deal result = new Deal( getDealID(), getDealTime(), cp , instru, getDealType(), getDealAmount(), getDealQuantity() );
	       result.getDealCounterpartyID().setCounterPartyName(getCounterPartyName());
	       result.getDealCounterpartyID().setCounterPartyStatus(getCounterPartyStatus());
	       result.getDealCounterpartyID().setCounterPartyDateRegistered(getCounterPartyDateRegistered());
	       result.getDealInstrumentID().setInstrumentName(getInstrumentName());
	       return result;
	   }
	
}
