package deutschebank.dbutils;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CounterPartyIterator {
	   ResultSet rowIterator;

	   CounterPartyIterator( ResultSet rs )
	   {
	       rowIterator = rs;
	   }
	   
	   public boolean  first() throws SQLException
	   {
	      return rowIterator.first();
	   }
	   
	   public boolean last() throws SQLException
	   {
	      return rowIterator.last();
	   }
	   public boolean next() throws SQLException
	   {
	      return rowIterator.next();
	   }
	   
	   public boolean prior() throws SQLException
	   {
	      return rowIterator.previous();
	   }

	   public   int  getCounterPartyID() throws SQLException
	   {
	      return rowIterator.getInt("counterparty_id");
	   }

	   public   String  getCounterPartyName() throws SQLException
	   {
	      return rowIterator.getString("counterparty_name");
	   }
	   
	   public   String  getCounterPartyStatus() throws SQLException
	   {
	      return rowIterator.getString("counterparty_status");
	   }
	   
	   public   String  getCounterPartyDateRegistered() throws SQLException
	   {
	      return rowIterator.getString("counterparty_date_registered");
	   }

	   CounterParty buildCounterParty() throws SQLException
	   {
		   CounterParty result = new CounterParty(getCounterPartyID(), getCounterPartyName(), getCounterPartyStatus(), getCounterPartyDateRegistered());
	       
	       return result;
	   }
}
