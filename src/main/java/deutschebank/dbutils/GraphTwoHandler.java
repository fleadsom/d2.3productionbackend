/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deutschebank.dbutils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import deutschebank.MainUnit;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Selvyn
 */
public class GraphTwoHandler
{
    static  private GraphTwoHandler itsSelf = null;
    
    private GraphTwoHandler(){}
    
    static  public  GraphTwoHandler  getLoader()
    {
        if( itsSelf == null )
            itsSelf = new GraphTwoHandler();
        return itsSelf;
    }
    
    public  ArrayList<GraphTwo>  loadFromDB( String dbID, Connection theConnection )
    {
        ArrayList<GraphTwo> result = new ArrayList<GraphTwo>();
        GraphTwo theGraphTwo = null;
        try
        {
            String sbQuery = "select instr.instrument_name, d.deal_time, d.deal_amount, d.deal_quantity";
            sbQuery = sbQuery  + " from "+dbID +".instrument instr join "+dbID +".deal d on";
            sbQuery = sbQuery  + " instr.instrument_id=d.deal_instrument_id";
            
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);            
            ResultSet rs = stmt.executeQuery();
            
            GraphTwoIterator iter = new GraphTwoIterator(rs);
            
            while( iter.next() )
            {
            	theGraphTwo = iter.buildGraphTwo();
                if(MainUnit.debugFlag)
                    System.out.println( theGraphTwo.getSymbol() + "//" + theGraphTwo.getDate() );
                result.add(theGraphTwo);
            }
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(GraphTwoHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }

}
