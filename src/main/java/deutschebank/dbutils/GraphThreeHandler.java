/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deutschebank.dbutils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import deutschebank.MainUnit;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Selvyn
 */
public class GraphThreeHandler
{
    static  private GraphThreeHandler itsSelf = null;
    
    private GraphThreeHandler(){}
    
    static  public  GraphThreeHandler  getLoader()
    {
        if( itsSelf == null )
            itsSelf = new GraphThreeHandler();
        return itsSelf;
    }
    
    public  ArrayList<GraphThree>  loadFromDB( String dbID, Connection theConnection , String counterPartyName)
    {
        ArrayList<GraphThree> result = new ArrayList<GraphThree>();
        GraphThree theGraphThree = null;
        try
        {
            String sbQuery = "select counterparty_name, deal_type, instrument_name,  deal_amount, deal_quantity, deal_id  ";
            sbQuery = sbQuery  + "from "+dbID +".deal join "+dbID +".counterparty on deal_counterparty_id=counterparty_id ";
            sbQuery = sbQuery  + "join "+dbID +".instrument on deal_instrument_id=instrument_id where counterparty_name =?";
            sbQuery = sbQuery  + "group by counterparty_name, deal_type, instrument_name, deal_id, deal_amount, deal_quantity, deal_id";
            
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);  
            stmt.setString(1, counterPartyName);
            ResultSet rs = stmt.executeQuery();
            
            GraphThreeIterator iter = new GraphThreeIterator(rs);
            
            while( iter.next() )
            {
            	theGraphThree = iter.buildGraphThree();
                if(MainUnit.debugFlag)
                    System.out.println( theGraphThree.getDealQuantity() + "//" + theGraphThree.getDealType() );
                result.add(theGraphThree);
            }
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(GraphThreeHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }

}
