package deutschebank.dbutils;

public class GraphOne {
	
    private String  gOneInstrumentName;
    private String gOnecounterPartyName;
    private String gOnedealType;
    private double gOnedealAmount;
    private int gOnedealQuantity;
    
    public GraphOne(String gOneInstrumentName, String gOnecounterPartyName, String gOnedealType, double gOnedealAmount, int gOnedealQuantity ) {
    	this.gOneInstrumentName = gOneInstrumentName;
    	this.gOnecounterPartyName = gOnecounterPartyName;
    	this.gOnedealType = gOnedealType;
    	this.gOnedealAmount = gOnedealAmount;
    	this.gOnedealQuantity = gOnedealQuantity;
    }
    
    public String getInstrumentName()
    {
        return gOneInstrumentName;
    }

    public void setInstrumentName(String gOneInstrumentName)
    {
        this.gOneInstrumentName = gOneInstrumentName;
    }
    
    public String getCounterPartyName()
    {
        return gOnecounterPartyName;
    }

    public void setCounterPartyName(String gOnecounterPartyName)
    {
        this.gOnecounterPartyName = gOnecounterPartyName;
    }
    
    public String getDealType()
    {
        return gOnedealType;
    }

    public void setDealType(String gOnedealType)
    {
        this.gOnedealType = gOnedealType;
    }
    
    public double getDealAmount()
    {
        return gOnedealAmount;
    }

    public void setDealAmount(double gOnedealAmount)
    {
        this.gOnedealAmount = gOnedealAmount;
    }
    
    public int getDealQuantity()
    {
        return gOnedealQuantity;
    }

    public void setDealQuantity(int gOnedealQuantity)
    {
        this.gOnedealQuantity = gOnedealQuantity;
    }
    
    @Override
    public String toString() {
    	return "gOneInstrumentName: " + getInstrumentName() + "\tgOnecounterPartyName: " + getCounterPartyName() + "\tgOnedealType: [" + getDealType() + "]\tgOnedealAmount: [" + getDealAmount() +"]\tgetDealQuantity: " + getDealQuantity() ;
    } 
    
}
