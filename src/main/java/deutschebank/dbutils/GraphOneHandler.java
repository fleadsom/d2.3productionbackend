/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deutschebank.dbutils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import deutschebank.MainUnit;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Selvyn
 */
public class GraphOneHandler
{
    static  private GraphOneHandler itsSelf = null;
    
    private GraphOneHandler(){}
    
    static  public  GraphOneHandler  getLoader()
    {
        if( itsSelf == null )
            itsSelf = new GraphOneHandler();
        return itsSelf;
    }
    
    public  ArrayList<GraphOne>  loadFromDB( String dbID, Connection theConnection )
    {
        ArrayList<GraphOne> result = new ArrayList<GraphOne>();
        GraphOne theGraphOne = null;
        try
        {
            String sbQuery = "select instr.instrument_name, cp.counterparty_name, d.deal_type, d.deal_amount, d.deal_quantity from " + dbID;
            sbQuery = sbQuery  + ".deal d join "+dbID +".instrument instr on d.deal_instrument_id=instr.instrument_id join "+dbID;
            sbQuery = sbQuery  + ".counterparty cp on d.deal_counterparty_id= cp.counterparty_id ";
            sbQuery = sbQuery  + "group by instr.instrument_name, cp.counterparty_name, d.deal_type, d.deal_amount, d.deal_quantity";
            
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);            
            ResultSet rs = stmt.executeQuery();
            
            GraphOneIterator iter = new GraphOneIterator(rs);
            
            while( iter.next() )
            {
            	theGraphOne = iter.buildGraphOne();
                if(MainUnit.debugFlag)
                    System.out.println( theGraphOne.getInstrumentName() + "//" + theGraphOne.getCounterPartyName() );
                result.add(theGraphOne);
            }
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(GraphOneHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
    
    public  ArrayList<GraphOne>  loadFromDB( String dbID, Connection theConnection, String instrumentName )
    {
        ArrayList<GraphOne> result = new ArrayList<GraphOne>();
        GraphOne theGraphOne = null;
        try
        {
            String sbQuery = "select instr.instrument_name, cp.counterparty_name, d.deal_type, d.deal_amount, d.deal_quantity from " + dbID;
            sbQuery = sbQuery  + ".deal d join "+dbID +".instrument instr on d.deal_instrument_id=instr.instrument_id join "+dbID;
            sbQuery = sbQuery  + ".counterparty cp on d.deal_counterparty_id= cp.counterparty_id ";
            sbQuery = sbQuery  + "where instr.instrument_name=? ";
            sbQuery = sbQuery  + "group by instr.instrument_name, cp.counterparty_name, d.deal_type, d.deal_amount, d.deal_quantity";
           
            		
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery); 
            stmt.setString(1, instrumentName);
            ResultSet rs = stmt.executeQuery();
            
            GraphOneIterator iter = new GraphOneIterator(rs);
            
            while( iter.next() )
            {
            	theGraphOne = iter.buildGraphOne();
                if(MainUnit.debugFlag)
                    System.out.println( theGraphOne.getInstrumentName() + "//" + theGraphOne.getCounterPartyName() );
                result.add(theGraphOne);
            }
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(GraphOneHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }

    public  String  toJSON( ArrayList<Instrument> theInstruments )
    {
        String result = "";
        Instrument[] insArray = new Instrument[theInstruments.size()];
        theInstruments.toArray(insArray);
        try
        {
            ObjectMapper mapper = new ObjectMapper();
            result = mapper.writeValueAsString(insArray);
        } 
        catch (JsonProcessingException ex)
        {
            Logger.getLogger(GraphOneHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex)
        {
            Logger.getLogger(GraphOneHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}
