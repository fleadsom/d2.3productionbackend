package deutschebank.dbutils;

import java.util.ArrayList;

public class JsonFormat {
	
    private String name;
    private ArrayList<JsonFormat> children;
   
    
    public JsonFormat() {
    }
    
    public JsonFormat(String name, ArrayList<JsonFormat> children ) {
    	this.name = name;
    	this.children = children;
    }
    
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
    
    public ArrayList<JsonFormat> getChildren()
    {
        return children;
    }

    public void setChildren(ArrayList<JsonFormat> children)
    {
        this.children = children;
    }
    
    
    
    @Override
    public String toString() {
    	return "Name: " + getName() + "\tChildren: " + getChildren() ;
    } 
    
}
