package deutschebank.dbutils;

public class GraphTwo {
	
    private String symbol;
    private String date;
    private double price;
    
    public GraphTwo(String symbol, String date, double price ) {
    	this.symbol = symbol;
    	this.date = date;
    	this.price = price;
    }
    
    public String getSymbol()
    {
        return symbol;
    }

    public void setSymbol(String symbol)
    {
        this.symbol = symbol;
    }
    
    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }
    
    public double getPrice()
    {
        return price;
    }

    public void setPrice(double price)
    {
        this.price = price;
    }
    
    
    @Override
    public String toString() {
    	return "symbol: " + getSymbol() + "\tgdate: " + getDate() + "\tgprice: [" + getPrice() + "]";
    } 
    
}
