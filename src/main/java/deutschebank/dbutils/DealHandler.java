package deutschebank.dbutils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import deutschebank.MainUnit;

public class DealHandler {

    static  private DealHandler itsSelf = null;
    
    private DealHandler(){}
    
    static  public  DealHandler  getLoader()
    {
        if( itsSelf == null )
            itsSelf = new DealHandler();
        return itsSelf;
    }
    
    public  ArrayList<Deal>  loadFromDB( String dbID, Connection theConnection, int key )
    {
    	ArrayList<Deal> result = new ArrayList<Deal>();
        Deal theDeal = null;
        try
        {
            String sbQuery = "select * from " + dbID + ".deal d join "+ dbID + ".counterparty cp on d.deal_counterparty_id=cp.counterparty_id join "+ dbID + ".instrument instr on d.deal_instrument_id and instr.instrument_id where d.deal_id=?";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);
            stmt.setInt(1, key);
            ResultSet rs = stmt.executeQuery();
            
            DealIterator iter = new DealIterator(rs);
            
            while( iter.next() )
            {
            	theDeal = iter.buildDeal();
                
                    System.out.println( theDeal.getDealID() + "//" + theDeal.getDealTime() );
                result.add(theDeal);
            }
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(DealHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public  ArrayList<Deal>  loadFromDB( String dbID, Connection theConnection )
    {
        ArrayList<Deal> result = new ArrayList<Deal>();
        Deal theDeal = null;
        try
        {
            String sbQuery = "select * from " + dbID + ".deal d join "+ dbID + ".counterparty cp on d.deal_counterparty_id=cp.counterparty_id join "+ dbID + ".instrument instr on d.deal_instrument_id and instr.instrument_id";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);            
            ResultSet rs = stmt.executeQuery();
            
            DealIterator iter = new DealIterator(rs);
            
            while( iter.next() )
            {
            	theDeal = iter.buildDeal();
                if(MainUnit.debugFlag)
                    System.out.println( theDeal.getDealID() + "//" + theDeal.getDealTime() );
                result.add(theDeal);
            }
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(DealHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
    
    public  ArrayList<Deal>  loadFromDB( String dbID, Connection theConnection, String top)
    {
        ArrayList<Deal> result = new ArrayList<Deal>();
        Deal theDeal = null;
        try
        {
            String sbQuery = "select * from " + dbID + ".deal d join "+ dbID + ".counterparty cp on d.deal_counterparty_id=cp.counterparty_id join "+ dbID + ".instrument instr on d.deal_instrument_id and instr.instrument_id limit "+top;
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);            
            ResultSet rs = stmt.executeQuery();
            
            DealIterator iter = new DealIterator(rs);
            
            while( iter.next() )
            {
            	theDeal = iter.buildDeal();
                if(MainUnit.debugFlag)
                    System.out.println( theDeal.getDealID() + "//" + theDeal.getDealTime() );
                result.add(theDeal);
            }
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(DealHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
    
    public  String  toJSON( Deal theDeal )
    {
        String result = "";
        try
        {
            ObjectMapper mapper = new ObjectMapper();
            result = mapper.writeValueAsString(theDeal);
        } 
        catch (JsonProcessingException ex)
        {
            Logger.getLogger(DealHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex)
        {
            Logger.getLogger(DealHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public  String  toJSON( ArrayList<Instrument> theDeals )
    {
        String result = "";
        Instrument[] insArray = new Instrument[theDeals.size()];
        theDeals.toArray(insArray);
        try
        {
            ObjectMapper mapper = new ObjectMapper();
            result = mapper.writeValueAsString(insArray);
        } 
        catch (JsonProcessingException ex)
        {
            Logger.getLogger(DealHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex)
        {
            Logger.getLogger(DealHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}
