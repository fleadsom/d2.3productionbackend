package deutschebank.dbutils;

import java.util.Comparator;

public class Deal {
	
	private int dealID;
    private String dealTime;
    private CounterParty dealCounterpartyID;
    private Instrument dealInstrumentID;
    private String dealType;
    private double dealAmount;
    private int dealQuantity;
    
    public  Deal( int dealID, String dealTime, CounterParty dealCounterpartyID, Instrument dealInstrumentID, String dealType, double dealAmount, int dealQuantity )
    {
    	this.dealID = dealID;
    	this.dealTime = dealTime;
    	this.dealCounterpartyID = dealCounterpartyID;
    	this.dealInstrumentID = dealInstrumentID;
    	this.dealType = dealType;
    	this.dealAmount = dealAmount;
    	this.dealQuantity = dealQuantity;
    }

    public int getDealID()
    {
        return dealID;
    }

    public void setDealID(int dealID)
    {
        this.dealID = dealID;
    }
    
    public String getDealTime()
    {
        return dealTime;
    }

    public void setDealTime(String dealTime)
    {
        this.dealTime = dealTime;
    }
    
    public CounterParty getDealCounterpartyID()
    {
        return dealCounterpartyID;
    }

    public void setDealCounterpartyID(CounterParty dealCounterpartyID)
    {
        this.dealCounterpartyID = dealCounterpartyID;
    }
    
    public Instrument getDealInstrumentID()
    {
        return dealInstrumentID;
    }

    public void setDealInstrumentID(Instrument dealInstrumentID)
    {
        this.dealInstrumentID = dealInstrumentID;
    }
    
    public String getDealType()
    {
        return dealType;
    }

    public void setDealType(String dealType)
    {
        this.dealType = dealType;
    }
    
    public double getDealAmount()
    {
        return dealAmount;
    }

    public void setDealAmount(double dealAmount)
    {
        this.dealAmount = dealAmount;
    }
    
    public int getDealQuantity()
    {
        return dealQuantity;
    }

    public void setDealQuantity(int dealQuantity)
    {
        this.dealQuantity = dealQuantity;
    }
    
    @Override
    public String toString() {
    	return "dealID: " + dealID + "\tdealTime: " + dealTime + "\tdealCounterpartyID: [" + dealCounterpartyID + "]\tdealInstrumentID: [" + dealInstrumentID +"]\tdealType: " + dealType + "\tdealAmount: " + dealAmount + "\tdealQuantity: "+ dealQuantity;
    } 
    
    public static Comparator<Deal> DealTotalPriceComparator = new Comparator<Deal>() {

    	public int compare(Deal d1, Deal d2) {
    		double d1Total = d1.getDealAmount() * d1.getDealQuantity();
    		double d2Total = d2.getDealAmount() * d2.getDealQuantity();
    		
    		return (int) (d2Total-d1Total);

        }};
        
        
    public static Comparator<Deal> DealTimeComparator = new Comparator<Deal>() {

        	public int compare(Deal d1, Deal d2) {
        		String d1Total = d1.getDealTime();
        		String d2Total = d2.getDealTime();
        		

        	   return d1Total.compareTo(d2Total);

            }};
            
       
}
