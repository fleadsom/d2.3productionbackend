package deutschebank.dbutils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import deutschebank.MainUnit;

public class CounterPartyHandler {
	static  private CounterPartyHandler itsSelf = null;
    
    private CounterPartyHandler(){}
    
    static  public  CounterPartyHandler  getLoader()
    {
        if( itsSelf == null )
            itsSelf = new CounterPartyHandler();
        return itsSelf;
    }
    
    public  ArrayList<CounterParty>  loadFromDB( String dbID, Connection theConnection )
    {
        ArrayList<CounterParty> result = new ArrayList<CounterParty>();
        CounterParty theCounterParty = null;
        try
        {
            String sbQuery = "select * from " + dbID + ".counterparty";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);            
            ResultSet rs = stmt.executeQuery();
            
            CounterPartyIterator iter = new CounterPartyIterator(rs);
            
            while( iter.next() )
            {
            	theCounterParty = iter.buildCounterParty();
                if(MainUnit.debugFlag)
                    System.out.println( theCounterParty.getCounterPartyID() + "//" + theCounterParty.getCounterPartyName());
                result.add(theCounterParty);
            }
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(CounterPartyHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
    
    
}
