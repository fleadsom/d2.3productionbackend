package deutschebank.dbutils;

public class CounterParty 
{
	private int counterPartyID;
    private String counterPartyName;
    private String counterPartyStatus;
    private String counterPartyDateRegistered;
    
    public  CounterParty( int counterPartyID) {
    	this.counterPartyID = counterPartyID;
    }
    public  CounterParty( int counterPartyID, String counterPartyName, String counterPartyStatus, String counterPartyDateRegistered)
    {
    	this.counterPartyID = counterPartyID;
    	this.counterPartyName = counterPartyName;
    	this.counterPartyStatus = counterPartyStatus;
    	this.counterPartyDateRegistered = counterPartyDateRegistered;
    }

    public int getCounterPartyID()
    {
        return counterPartyID;
    }

    public void setCounterPartyID(int counterPartyID)
    {
        this.counterPartyID = counterPartyID;
    }
    
    public String getCounterPartyName()
    {
        return counterPartyName;
    }

    public void setCounterPartyName(String counterPartyName)
    {
        this.counterPartyName = counterPartyName;
    }
    
    public String getCounterPartyStatus()
    {
        return counterPartyStatus;
    }

    public void setCounterPartyStatus(String counterPartyStatus)
    {
        this.counterPartyStatus = counterPartyStatus;
    }
    
    public String getCounterPartyDateRegistered()
    {
        return counterPartyDateRegistered;
    }

    public void setCounterPartyDateRegistered(String counterPartyDateRegistered)
    {
        this.counterPartyDateRegistered = counterPartyDateRegistered;
    }
    
    @Override
    public String toString() {
    	return "counterPartyID: " + counterPartyID + " counterPartyName: " + counterPartyName + " counterPartyStatus: " + counterPartyStatus + " counterPartyDateRegistered: " + counterPartyDateRegistered;
    }  
}
