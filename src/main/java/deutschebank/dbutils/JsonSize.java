package deutschebank.dbutils;

import java.util.ArrayList;

public class JsonSize extends JsonFormat{
	
    private String name;
    private double size;
   
    
    public JsonSize() {
    }
    
    public JsonSize(String name, double size ) {
    	this.name = name;
    	this.size = size;
    }
    
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
    
    public double getSize()
    {
        return size;
    }

    public void setSize(double size)
    {
        this.size = size;
    }
    
    
    
    @Override
    public String toString() {
    	return "Name: " + getName() + "\tSize: " + getSize() ;
    } 
    
}
