/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deutschebank.dbutils;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Selvyn
 */
public class GraphThreeIterator
{
   ResultSet rowIterator;

   GraphThreeIterator( ResultSet rs )
   {
       rowIterator = rs;
   }
   
   public boolean  first() throws SQLException
   {
      return rowIterator.first();
   }
   
   public boolean last() throws SQLException
   {
      return rowIterator.last();
   }
   public boolean next() throws SQLException
   {
      return rowIterator.next();
   }
   
   public boolean prior() throws SQLException
   {
      return rowIterator.previous();
   }
   
   public   int  getDealQuantity() throws SQLException
   {
      return rowIterator.getInt("deal_quantity");
   }
   
   public   String  getDealType() throws SQLException
   {
      return rowIterator.getString("deal_type");
   }
   
   public   double  getDealAmount() throws SQLException
   {
      return rowIterator.getDouble("deal_amount");
   }
   
   public   String  getInstrumentName() throws SQLException
   {
      return rowIterator.getString("instrument_name");
   }
   
   public   String  getCounterPartyName() throws SQLException
   {
      return rowIterator.getString("counterparty_name");
   }
   
   public   int  getDealID() throws SQLException
   {
      return rowIterator.getInt("deal_id");
   }

   GraphThree   buildGraphThree() throws SQLException
   {
	   GraphThree result = new GraphThree( getDealQuantity(), getDealType(), getDealAmount(), getInstrumentName(), getCounterPartyName(), getDealID()  );
       return result;
   }
}
