package deutschebank.dbutils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/*
 * 
 * 
 *
		   
		   */
public class Common {
    public static  ArrayList<Deal> filterInstrumentName(ArrayList<Deal> deals, String filter){
    	ArrayList<Deal> filteredDeals = new ArrayList<Deal>();
    	deals.forEach( (deal)->
        {
        	System.out.println("Common cc "+deal.getDealInstrumentID().getInstrumentName());
        	if (deal.getDealInstrumentID().getInstrumentName().equals(filter)) {
        		filteredDeals.add(deal);
        	}
        }
        );
    	return filteredDeals;
    }
    
    public static ArrayList<Deal> filterCounterPartyName(ArrayList<Deal> arr, String filter){
    	ArrayList<Deal> filteredArr = new ArrayList<Deal>();	
    	arr.forEach( (deal)->
        {
        	if (deal.getDealCounterpartyID().getCounterPartyName().equals(filter)) {
        		filteredArr.add(deal);
        	}      
        }
        );
    	return filteredArr;
    }
    
    public static ArrayList<Deal> filterCounterPartyStatus(ArrayList<Deal> arr, String filter){
    	ArrayList<Deal> filteredArr = new ArrayList<Deal>();	
    	arr.forEach( (deal)->
        {
        	if (deal.getDealCounterpartyID().getCounterPartyStatus().equals(filter)) {
        		filteredArr.add(deal);
        	}     
        }
        );
    	return filteredArr;
    }
    
    public static ArrayList<Deal> filterCounterPartyDateRegistered(ArrayList<Deal> arr, String from, String to) throws ParseException{
    	ArrayList<Deal> filteredArr = new ArrayList<Deal>();
    	SimpleDateFormat pattern = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    	Date fromDate = pattern.parse(from);
    	Date toDate = pattern.parse(to);
    		
    	arr.forEach( (deal)->
        {
        	  Date d = null;
			try {
				d = pattern.parse(deal.getDealCounterpartyID().getCounterPartyDateRegistered());
			} catch (ParseException e) {
				e.printStackTrace();
			}
        	  if ((d.after(fromDate) || d.equals(fromDate)) && (d.before(toDate) || d.equals(toDate))) {
        		  filteredArr.add(deal);
        	  }
        }
        );
    	
    	return filteredArr;
    }
}
