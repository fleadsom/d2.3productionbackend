package deutschebank.dbutils;


public class GraphThree {
	
	private int dealQuantity;
	private String dealType;
	private double dealAmount;
	private String  instrumentName;
	private String counterPartyName;
	private int dealID;
	
    public GraphThree(int dealQuantity, String dealType, double dealAmount, String  instrumentName, String counterPartyName, int dealID ) {
    	this.dealQuantity = dealQuantity;
    	this.dealType = dealType;
    	this.dealAmount = dealAmount;
    	this.instrumentName = instrumentName;
    	this.counterPartyName = counterPartyName;
    	this.dealID = dealID;
    }
    
    public int getDealQuantity()
    {
        return dealQuantity;
    }

    public void setDealQuantity(int dealQuantity)
    {
        this.dealQuantity = dealQuantity;
    }
    
    public String getDealType()
    {
        return dealType;
    }

    public void setDealType(String dealType)
    {
        this.dealType = dealType;
    }
    
    public double getDealAmount()
    {
        return dealAmount;
    }

    public void setDealAmount(double dealAmount)
    {
        this.dealAmount = dealAmount;
    }
    
    public String getinstrumentName()
    {
        return instrumentName;
    }

    public void setinstrumentName(String instrumentName)
    {
        this.instrumentName = instrumentName;
    }
    
    public String getcounterPartyName()
    {
        return counterPartyName;
    }

    public void setcounterPartyName(String counterPartyName)
    {
        this.counterPartyName = counterPartyName;
    }
    
    public int getDealID()
    {
        return dealID;
    }

    public void setDealID(int dealID)
    {
        this.dealID = dealID;
    }
    
    
    @Override
    public String toString() {
    	return "DealQuantity: " + getDealQuantity() + "\tgDealType: " + getDealType() + "\tgDealAmount: [" + getDealAmount() + "]";
    } 
    
}
