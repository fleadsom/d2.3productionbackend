/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deutschebank;

import deutschebank.core.Controller;
import deutschebank.dbutils.Common;
import deutschebank.dbutils.CounterParty;
import deutschebank.dbutils.CounterPartyHandler;
import deutschebank.dbutils.DBConnector;
import deutschebank.dbutils.Deal;
import deutschebank.dbutils.DealHandler;
import deutschebank.dbutils.GraphOne;
import deutschebank.dbutils.GraphOneHandler;
import deutschebank.dbutils.Instrument;
import deutschebank.dbutils.InstrumentHandler;
import deutschebank.dbutils.JsonFormat;
import deutschebank.dbutils.JsonSize;
import deutschebank.dbutils.PropertyLoader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.fasterxml.jackson.databind.ObjectMapper;
import deutschebank.dbutils.User;
import deutschebank.dbutils.UserHandler;
import java.io.File;
import java.util.logging.FileHandler;

/**
 *
 * @author Selvyn
 */

/*
 * first graph
 * select instrument_name, counterparty_name, deal_type, deal_amount, deal_quantity
from deal join instrument on deal_instrument_id=instrument.instrument_id join
counterparty on deal_counterparty_id=counterparty_id
group by instrument_name, counterparty_name, deal_type, deal_amount, deal_quantity;

second graph
select counterparty_name, deal_type, instrument_name, deal_id, deal_amount, deal_quantity
from deal join instrument on deal_instrument_id=instrument.instrument_id join
counterparty on deal_counterparty_id=counterparty_id
where counterparty_name="Estelle" and deal_type="B"
group by counterparty_name, deal_type,instrument_name, deal_id, deal_amount, deal_quantity;

third graph
select instrument_name, deal_time, deal_amount, deal_quantity
from instrument join deal on
instrument_id=deal_instrument_id;
 */
public class MainUnit
{
   private static final Logger LOGGER = Logger.getLogger(LoggerExample.class.getName());
   public  static  boolean debugFlag = false; //true
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        try
        {
        	

        	/*SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
        	Date dNow = new Date();
        	LOGGER.addHandler(new FileHandler(new File ("./log/dbCoreLog-"+ ft.format(dNow).toString()+".log").getName()));
        	*/
            DBConnector connector = DBConnector.getConnector();
            PropertyLoader pLoader = PropertyLoader.getLoader();
            Properties pp = pLoader.getPropValues( "dbConnector.properties" );
            connector.connect( pp );
            
            //Controller ct = new Controller ();
        	//ct.getGraphTwo();
            /*
            JsonFormat jInstrument = new JsonFormat(); 
            JsonFormat jInstrumentName = new JsonFormat();
            JsonFormat jCounterPartyName = new JsonFormat(); 
             
            ArrayList<JsonFormat> jInstrumentNameArr = new ArrayList<JsonFormat>();
            ArrayList<JsonFormat> jCounterPartyNameArr = new ArrayList<JsonFormat>();
            ArrayList<JsonSize> jDealArr = new ArrayList<JsonSize>();
            
            GraphOneHandler theGraphOneHandler = GraphOneHandler.getLoader();
            ArrayList<GraphOne> theGraphOne = theGraphOneHandler.loadFromDB(pp.getProperty("dbName"), connector.getConnection());
            //System.out.println( "before"+ theGraphOne.size() );
            String nowInstrument = "";
            String nowCounterParty = "";
            String nowInstrumentCP = "";
            for(GraphOne g1: theGraphOne){
 				//System.out.println(g1.toString());
 				if(!nowInstrument.equals(g1.getInstrumentName())) {
 					nowInstrument = g1.getInstrumentName();
 					JsonFormat addInstrumentName = new JsonFormat();
 					addInstrumentName.setName(g1.getInstrumentName());
 					jInstrumentNameArr.add(addInstrumentName);
 				} 
 		   }
            GraphOneHandler theGraphOneHandlerCP = GraphOneHandler.getLoader();
            
            ArrayList<GraphOne> theGraphOneCP = theGraphOneHandlerCP.loadFromDB(pp.getProperty("dbName"), connector.getConnection() ,"Astronomica");
            System.out.println( "before"+ theGraphOneCP.size() );
            System.out.println( "before"+ theGraphOneCP.toString() );
            for(GraphOne gCP: theGraphOneCP){
            	if(!nowCounterParty.equals(gCP.getCounterPartyName())) {
            	JsonFormat jCounterPartyNameCP = new JsonFormat();
            	jCounterPartyNameCP.setName(gCP.getCounterPartyName());
            	jInstrumentNameArr.add(jCounterPartyNameCP);
            	}
            	
            }
            
            for(JsonFormat jf: jInstrumentNameArr){
 				System.out.println(jf.toString());
 		    }
            /*
            for(GraphOne g1: theGraphOne){ 
            	if ( ( nowInstrumentCP.equals("") || nowInstrumentCP.equals("") )&&!nowCounterParty.equals(g1.getCounterPartyName())) {
					System.out.println("in");
					nowCounterParty = g1.getCounterPartyName();
					JsonFormat addCounterPartyName = new JsonFormat();
					addCounterPartyName.setName(g1.getCounterPartyName());
					jCounterPartyNameArr.add(addCounterPartyName);
					jInstrumentName.setChildren(jCounterPartyNameArr);
				} 
					JsonSize addJsonSize = new JsonSize();
					addJsonSize.setName(g1.getDealType());
					addJsonSize.setSize(g1.getDealAmount()/g1.getDealQuantity());
					jDealArr.add(addJsonSize);
            }
            */
           //jCounterPartyNameArr.add(jCounterPartyName); 
          // jCounterPartyName.setName("Lina");
          // jCounterPartyName.setChildren(jDealArr);	
          // jInstrumentName.setName("Astronomica");
           /*
           jInstrument.setName("Instrument");
           jInstrument.setChildren(jInstrumentNameArr);
           
           ObjectMapper mapper = new ObjectMapper();
           String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jInstrument);
           System.out.println(jsonInString);
           */
            /*
            CounterPartyHandler theCounterPartyHandler = CounterPartyHandler.getLoader();
            ArrayList<CounterParty> theCounterPartys = theCounterPartyHandler.loadFromDB(pp.getProperty("dbName"), connector.getConnection());
            
            ObjectMapper mapper = new ObjectMapper();
            String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(theCounterPartys);
            System.out.println(jsonInString);
           
            InstrumentHandler theInstrumentHandler = InstrumentHandler.getLoader();
            ArrayList<Instrument> theInstruments = theInstrumentHandler.loadFromDB(pp.getProperty("dbName"), connector.getConnection());
            
            ObjectMapper mapper = new ObjectMapper();
            String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(theInstruments);
            System.out.println(jsonInString);
             */
            //Controller ct = new Controller ();
        	//ct.getRequirementOne("S");
        	/*
            DealHandler theDealHandler = DealHandler.getLoader();
            ArrayList<Deal> theDeals = theDealHandler.loadFromDB(pp.getProperty("dbName"), connector.getConnection(), "100");
            System.out.println( "before"+ theDeals.size() );
            for(Deal d: theDeals){
 				System.out.println(d.toString());
 		   }*/
            /*
            ArrayList<Deal> theDealsf=Common.filterInstrumentName(theDeals, "Astronomica");
            System.out.println( theDealsf.size() );
            theDealsf.forEach( (deal)->
                     {
                         System.out.println( deal.toString() );
                     }
            );
            */
            /*
            ArrayList<Deal> theDealsn=Common.filterCounterPartyName(theDeals, "Richard");
            System.out.println( theDealsn.size() );
            ArrayList<Deal> theDealss=Common.filterCounterPartyStatus(theDeals, "A");
            System.out.println( theDealss.size() );
            ArrayList<Deal> theDealsd=null;
			try {
				theDealsd = Common.filterCounterPartyDateRegistered(theDeals, "2014-01-01 00:00:00", "2016-01-01 00:00:00");
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            System.out.println( theDealsd.size() );
            */
            
            System.out.println("=====================================================================");
            
          Controller ct = new Controller ();
        	String remes = ct.getGraphThree("Selvyn");
            System.out.println(remes);
            /*
            Collections.sort(theDeals, Deal.DealTotalPriceComparator);
            for(Deal d: theDeals){
 				System.out.println(d.toString());
 		   }
            */
            /*
 	       Collections.sort(theDeals, Deal.DealTimeComparator);
 	       for(Deal d: theDeals){
 				System.out.println(d.toString());
 		   }
            */
             
            /*
            ObjectMapper mapper = new ObjectMapper();
            String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(theDeals);
            System.out.println(jsonInString);
            
            InstrumentHandler theInstrumentHandler = InstrumentHandler.getLoader();
            Instrument theInstrument = theInstrumentHandler.loadFromDB(pp.getProperty("dbName"), connector.getConnection(), 2);
            
            if( theInstrument != null )
            {
                System.out.println( theInstrument.getInstrumentID() + "//" + theInstrument.getInstrumentName() );
            }

            ArrayList<Instrument> theInstruments = theInstrumentHandler.loadFromDB(pp.getProperty("dbName"), connector.getConnection(), 2, 10);
            
            Instrument[] insArray = new Instrument[theInstruments.size()];
            theInstruments.toArray(insArray);
            theInstruments.forEach( (instrument)->
                {
                    System.out.println( instrument.getInstrumentID() + "//" + instrument.getInstrumentName() );
                }
            );
            
            // Now convert the Instrument instane into a JSON object
            ObjectMapper mapper = new ObjectMapper();
            // Convert object to JSON string and save into a file directly
            mapper.writeValue(new File("instrument.json"), theInstrument);

            // Convert an array of objects to JSON string and save into a file directly
            mapper.writeValue(new File("instrument_array.json"), insArray);

            // Convert object to JSON string
            String jsonInString = mapper.writeValueAsString(theInstrument);
            System.out.println(jsonInString);

            // Convert object to JSON string and pretty print
            jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(theInstrument);
            System.out.println(jsonInString);

            //========================================================================
            // Working with user table
            //========================================================================
            UserHandler theUserHandler = UserHandler.getLoader();
            User theUser = theUserHandler.loadFromDB(pp.getProperty("dbName"), connector.getConnection(), "selvyn", "gradprog2016");
            
            if( theUser != null )
            {
                System.out.println( theUser.getUserID()+ "//" + theUser.getUserPwd());
            }

            // Convert object to JSON string and save into a file directly
            mapper.writeValue(new File("user.json"), theUser);
*/
        } 
        catch (IOException ex)
        {
            Logger.getLogger(MainUnit.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public  static  void log( String msg )
    {
        //if( debugFlag )
        {
            LOGGER.info( msg );
            System.out.println( msg );
        }
    }
    
}
